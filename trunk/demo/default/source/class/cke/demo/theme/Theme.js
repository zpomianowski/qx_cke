/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("cke.demo.theme.Theme",
{
  meta :
  {
    color : cke.demo.theme.Color,
    decoration : cke.demo.theme.Decoration,
    font : cke.demo.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : cke.demo.theme.Appearance
  }
});