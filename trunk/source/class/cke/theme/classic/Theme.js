/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("cke.theme.classic.Theme",
{
  meta :
  {
    color : cke.theme.classic.Color,
    decoration : cke.theme.classic.Decoration,
    font : cke.theme.classic.Font,
    appearance : cke.theme.classic.Appearance,
    icon : qx.theme.icon.Oxygen
  }
});