/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("cke.theme.modern.Theme",
{
  meta :
  {
    color : cke.theme.modern.Color,
    decoration : cke.theme.modern.Decoration,
    font : cke.theme.modern.Font,
    appearance : cke.theme.modern.Appearance,
    icon : qx.theme.icon.Tango
  }
});