/* ************************************************************************
@ignore(CKEDITOR*)
@asset(cke/*)

************************************************************************ */

qx.Class.define("cke.Editor", {
    extend: qx.ui.core.Widget,

    include: [qx.ui.form.MForm],
    implement: [qx.ui.form.IForm, qx.ui.form.IStringForm],

    construct: function()
    {
        this.base(arguments);

        var codeArr = [
            "ckeditor.js"
        ];

        //this.__addCss("ckeditor.css");
        this.__loadScriptArr(codeArr,qx.lang.Function.bind(this.__loadElement,this));
    },

    statics : {
        INSTANCE_COUNTER : 0,
        LOADED: {},
        LOADING: {}
    },

    events : {
        scriptLoaded: 'qx.event.type.Event'
    },

    properties:
    {
        appearance:
        {
            refine: true,
            init: "textarea"
        },

        value :
        {
            check : "String",
            nullable : true,
            event : "changeValue",
            init : ""
        },

        isLoaded:
        {
            nullable: true,
            init: false,
            apply: "__isLoaded"
        }
    },

    members : {
        __ckeditor: null,
      
        __loadScriptArr: function(codeArr,handler){
            var script = codeArr.shift();
            if (script){
                if (cke.Editor.LOADING[script]){
                    cke.Editor.LOADING[script].addListenerOnce('scriptLoaded',function(){
                        this.__loadScriptArr(codeArr,handler);
                    },this);
                }
                else if ( cke.Editor.LOADED[script]){
                     this.__loadScriptArr(codeArr,handler);
                }
                else {
                    cke.Editor.LOADING[script] = this;
                    var sl = new qx.io.ScriptLoader();
                    var src = qx.util.ResourceManager.getInstance().toUri("../ckeditor/"+script);
                    sl.load(src, function(status){
                        if (status == 'success'){
                            this.debug("Dynamically loaded "+src+": "+status);
                            this.__loadScriptArr(codeArr,handler);
                            cke.Editor.LOADED[script] = true;
                        }
                        cke.Editor.LOADING[script] = null;
                        this.fireDataEvent('scriptLoaded',script);
                    },this);
                }
            } else {
                handler();
            }
        },

        events : {
            afterCommandExec    : "qx.event.type.Data",
            afterPaste          : "qx.event.type.Data",
            afterSetData        : "qx.event.type.Data",
            afterUndoImage      : "qx.event.type.Data",
            beforeCommandExec   : "qx.event.type.Data",
            beforeGetData       : "qx.event.type.Data",
            beforeModeUnload    : "qx.event.type.Data",
            beforePaste         : "qx.event.type.Data",
            beforeSetMode       : "qx.event.type.Data",
            beforeUndoImage     : "qx.event.type.Data",
            blur                : "qx.event.type.Data",
            click               : "qx.event.type.Data",
            contentDirChanged   : "qx.event.type.Data",
            contentDom          : "qx.event.type.Data",
            contentDomUnload    : "qx.event.type.Data",
            customConfigLoaded  : "qx.event.type.Data",
            dataReady           : "qx.event.type.Data",
            //destroy             : "qx.event.type.Data",
            dirChanged          : "qx.event.type.Data",
            doubleclick         : "qx.event.type.Data",
            editingBlockReady   : "qx.event.type.Data",
            elementsPathUpdate  : "qx.event.type.Data",
            focus               : "qx.event.type.Data",
            getData             : "qx.event.type.Data",
            getSnapshot         : "qx.event.type.Data",
            insertElement       : "qx.event.type.Data",
            insertHtml          : "qx.event.type.Data",
            insertText          : "qx.event.type.Data",
            instanceDestroyed   : "qx.event.type.Data",
            instanceReady       : "qx.event.type.Data",
            key                 : "qx.event.type.Data",
            keydown             : "qx.event.type.Data",
            loadSnapshot        : "qx.event.type.Data",
            loaded              : "qx.event.type.Data",
            menuShow            : "qx.event.type.Data",
            mode                : "qx.event.type.Data",
            mousemove           : "qx.event.type.Data",
            mouseup             : "qx.event.type.Data",
            paste               : "qx.event.type.Data",
            pasteDialog         : "qx.event.type.Data",
            pasteState          : "qx.event.type.Data",
            readOnly            : "qx.event.type.Data",
            ready               : "qx.event.type.Data",
            removeFormatCleanup : "qx.event.type.Data",
            reset               : "qx.event.type.Data",
            resize              : "qx.event.type.Data",
            saveSnapshot        : "qx.event.type.Data",
            scaytDialog         : "qx.event.type.Data",
            scaytReady          : "qx.event.type.Data",
            selectionChange     : "qx.event.type.Data",
            showScaytState      : "qx.event.type.Data",
            themeLoaded         : "qx.event.type.Data",
            themeSpace          : "qx.event.type.Data",
            uiReady             : "qx.event.type.Data",
            updateSnapshot      : "qx.event.type.Data"
        },

        __addCss: function(url){
            if (!cke.Editor.LOADED[url]){
                cke.Editor.LOADED[url]=true;
                var head = document.getElementsByTagName("head")[0];
                var el = document.createElement("link");
                el.type = "text/css";
                el.rel="stylesheet";
                el.href=qx.util.ResourceManager.getInstance().toUri("../ckeditor/"+url);
                setTimeout(function() {
                    head.appendChild(el);
                }, 0);
            };
        },

        __loadElement: function() {
            //return;
            var el = this.getContentElement().getDomElement();
            if (el == null){
                this.addListenerOnce('appear', qx.lang.Function.bind(this.__loadElement,this),this);
            } else {
                cke.Editor.INSTANCE_COUNTER++;
                var el = this.getContentElement().getDomElement();
                el.setAttribute("id", "atms-ckeditor_" + cke.Editor.INSTANCE_COUNTER);
                el.setAttribute("contenteditable", "true");
                this.getContentElement().setStyle("overflowY", "auto");
               
                var hint = this.getBounds();

                CKEDITOR.disableAutoInline = true;
                CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
                CKEDITOR.config.toolbar = [
                    [ 'Paste', 'PasteText', 'PasteFromWord', 'Cut', 'Copy', '-', 'Undo', 'Redo' ],
                    [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
                    '/',
                    [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', '-', 'TextColor', 'BGColor' ],
                    [ 'Link']
                ];
                this.__ckeditor = CKEDITOR.inline(el.getAttribute("id"));
                var that = this;
                this.__ckeditor.on('instanceReady', function(event) {
                    var editor = event.editor;
                    var divElement = event.editor.element.$;

                    divElement.removeAttribute("title");
                    that.setIsLoaded(true);
                });
                
                this.addListener("destroy", this.__destroy);                
            }
        },

        __isLoaded: function(value, old, name) {
            this.__ckeditor.setData(this.getValue());
        },

        __destroy: function() {
            this.__ckeditor.destroy();
        },

        saveValue: function() {
            this.setValue(this.__ckeditor.getData());
        }
    }
});
